angular.module('starter.controllers', ['ngCordova'])
  .controller('AppCtrl', function ($scope, $http, stationsService) {
  })

  // imageHelper Factory simply sets the correct image icon with the correct station name. Used in both stationCtrl and stationsCtrl
  .factory ('imageHelper', function ($http){
    return {
      logo: function (name) {
        if (name == "Elemental") {
          return "img/elemental.png"
        }
        if (name == "Automobile Alley") {
          return "img/autoalley.png"
        }
        if (name == "COX Convention Center") {
          return "img/cox.jpg"
        }
        if (name == "Midtown Circle") {
          return "img/midtown.png"
        }
        if (name == "OKC Memorial ") {
          return "img/memorial.png"
        }
        if (name == "Bricktown Ballpark") {
          return "img/okcDodgers.png"
        }
        if (name == "Deep Deuce") {
          return "img/deepDeuce.png"
        }
        if (name == "Downtown Library ") {
          return "img/library.png"
        }
        else {
          return "img/midtown.jpg"
        }
      }
    }
})

  // This Factory is used to pull down and manipulate the json obj in order to add a distance field to the Spokies JSON obj.
  .factory('stationsService', function ($http, $q) {
    return {
      getStations: function (url) {
        console.log(url)
        var deferred = $q.defer();
        $http.get(url).then(function (resp) {
          console.log(resp.data.data.stations)
          // JSON object pulled down from the spokies webpage.
          var data = resp.data.data.stations;

          var onSuccess = function (position) {
            // Current Location Coords
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;

            for (var i = 0; i < data.length; i++) {
              // LatLng for the Station
              var latLng = new google.maps.LatLng(data[i].lat, data[i].lon);
              // The LatLng for the users current location
              var latLngb = new google.maps.LatLng(lat, lng);
              console.log(lat,lng)
              // Uses the google maps computeDistanceBetween to find units
              var dist = google.maps.geometry.spherical.computeDistanceBetween(latLngb, latLng)
              // Converts to units to miles and adds it to the JSON obj.
              data[i].distance = Math.round(dist / 1000 * 0.6214 * 10) / 10;
            }
            // Resolves the promise json object after the distance has been added.
            deferred.resolve(data);
          };
          // onError Callback receives a PositionError object
          function onError(error) {
            alert('code: ' + error.code + '\n' +
              'message: ' + error.message + '\n');
          }
          navigator.geolocation.getCurrentPosition(onSuccess, onError);
        })
        return deferred.promise;
      }
    }
  })

  // StationsCtrl pull down the station JSON data from spokiesokc.com and displays updated information
  .controller('StationsCtrl', function ($scope, $http, stationsService, imageHelper) {
    stationsService.getStations("https://gbfs.bcycle.com/bcycle_spokies/station_information.json")
      .then(function (data) {
        $http({
          method:'GET',
          url:'https://gbfs.bcycle.com/bcycle_spokies/station_status.json'
        }).then(function(response){

          $scope.availability = response.data

          $scope.getAvailability = function(station_id){
            console.log(station_id)
            return response.data[station_id]
          }


          console.log($scope.availability)
          // $scope.data = response.data
          //
          // $scope.details = response.data
          //success
          //do something with the response
        }, function(response){
          //error
          //show an appropriate message
        });

        $scope.stations = data;
        console.log(data)
        $scope.imageHelper = function(name){
           return imageHelper.logo(name)
         }
      }, function (err) {
        console.error('ERR', err);
        // err.status will contain the status code
      })
  })


  // mapCtrl pulls down the Spokies JSON data and plots all location on a map.
  .controller('mapCtrl', function ($scope, stationsService) {
    stationsService.getStations("https://gbfs.bcycle.com/bcycle_spokies/station_information.json")
      .then(function (data) {
        $scope.stations = data;
        //Initiates the info window for google maps marker location
        var infowindow = new google.maps.InfoWindow();
        // Bounds will center the map to show all the location markers
        var bounds = new google.maps.LatLngBounds();
        // latLng Just centers the map for the given plots
        var latLng = new google.maps.LatLng(data[0].Latitude, data[0].Longitude);
        // mapOptions are the maps options.
        var mapOptions = {
          center: latLng,
          zoom: 14,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        // Initializes the maps to display on the maps template as <div id="map_full">
        $scope.map2 = new google.maps.Map(document.getElementById("map_full"), mapOptions);
        // loop through the stations and build markers for the google map.
        for (var i = 0; i < data.length; i++) {
          var latlngc = new google.maps.LatLng(data[i].lat, data[i].lon);
          // LatLng for the Station in the json obj
          var marker = new google.maps.Marker({
            position: latlngc,
            map: $scope.map2,
            //label: 'Spokie Location',
            icon: {
              size: new google.maps.Size(72, 77),
              anchor: new google.maps.Point(21, 67),
              url: "http://spokiesokc.com/assets/images/marker.png?v=3"
            }
          });
          google.maps.event.addListener(marker, 'click', (function (marker, i) {
            var contentString = '<div id="content">'+
              '<div id="siteNotice">'+
              '</div>'+
              '<h3 class="energized">' + data[i].address +'</h3>'+
              '<div id="bodyContent">'+
              '<h5><i class="ion-android-bicycle balanced"> ' + data[i].Bikes + '</h5>' +
              '</div>'+
              '</div>';
            return function () {
              infowindow.setContent(contentString);
              infowindow.open($scope.map2, marker);
            }
          })(marker, i));
          bounds.extend(latlngc);
        }
        $scope.map2.fitBounds(bounds);

      }, function (err) {
        console.error('ERR', err);
        // err.status will contain the status code
      })
  })


  .controller ('directionsCtrl', function ($scope, $stateParams){
     directions.navigateTo($stateParams.latitude, $stateParams.longitude);
  })

  .controller('StationCtrl', function ($scope, $stateParams, $cordovaGeolocation, imageHelper) {
    $scope.station_id = $stateParams.stationId;
    $scope.station_name = $stateParams.stationName;
    $scope.latitude = $stateParams.stationLatitude;
    $scope.longitude =   $stateParams.stationLongitude;

    $scope.image = imageHelper.logo($stateParams.stationName);
    console.log($stateParams.stationName)


    var options = {timeout: 10000, enableHighAccuracy: true};

    //Fuction that will get current location
    // TODO - Need to handle error when current location isn't returned
    $cordovaGeolocation.getCurrentPosition(options).then(function (position) {
      var latLng = new google.maps.LatLng($stateParams.stationLatitude, $stateParams.stationLongitude);
      var latLngb = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      var mapOptions = {
        center: latLng,
        zoom: 17,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      // Setup the marker to display the given location on the map.
      var marker = new google.maps.Marker({
        position: latLng,
        map: map,
        label: 'Spokie Location',
        icon: {
          size: new google.maps.Size(72, 77),
          anchor: new google.maps.Point(21, 67),
          url: "http://spokiesokc.com/assets/images/marker.png?v=3"
        }
      });

      $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

      marker.setMap($scope.map);

      var dist = google.maps.geometry.spherical.computeDistanceBetween(latLngb, latLng)
      $scope.distance = Math.round(dist / 1000 * 0.6214 * 10) / 10;



    }, function (error) {
      console.log("Could not get location");
    });
  });
